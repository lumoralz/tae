library(shiny)
library(leaflet)
library(jsonlite)
library(lubridate)

theServer <- function(input, output, session){
  data <- fromJSON("https://www.medellin.gov.co/mapas/rest/services/ServiciosMovilidad/Accidentes/MapServer/12/query?where=1%3D1&outFields=*&outSR=4326&f=json")
  feat <- data$features
  
  updateSelectInput(session, "DISENO", choices=sort(unique(feat$attributes$DISENO)))
  updateSelectInput(session, "DIA", choices=sort(unique(feat$attributes$DIA)))
  updateSelectInput(session, "COMUNA", choices=sort(unique(feat$attributes$COMUNA)))
  updateSelectInput(session, "BARRIO", choices=sort(unique(feat$attributes$BARRIO)))
  
  cat <- c("Con muerto", "Muerto", "Herido", "Solo daños")
  col <- c("#000000", "#ff0000", "#ffa500", "#0000ff")
  
  edf <- list(geometry = data.frame(x = c(0), y = c(0)), popup = c(0))
  feat$popup <- paste("Fecha:",feat$attributes$DIA, ",", as.Date(as.POSIXct(feat$attributes$FECHA/1000, origin="1970-01-01")),
                      "<br/>Hora:", feat$attributes$HORA,
                      "<br/>Dirección:", feat$attributes$DIRECCION, "Barrio", feat$attributes$BARRIO,
                      "<br/>Comuna:", feat$attributes$COMUNA,
                      "<br/>Clase:", feat$attributes$CLASE, 
                      "<br/>Diseño:", feat$attributes$DISENO)
  feat$attributes$HORA <- parse_date_time(feat$attributes$HORA, '%I:%M %p')
  
  map <- leaflet() %>%
    addTiles(
      urlTemplate = "//{s}.tiles.mapbox.com/v3/jcheng.map-5ebohr46/{z}/{x}/{y}.png",
      attribution = 'Maps by <a href="http://www.mapbox.com/">Mapbox</a>'
    ) %>%
    setView(lng=-75.4965712, lat=6.2616968, zoom=12) %>%
    addLegend("bottomleft", title="Gravedad", pal=colorFactor(col, cat, ordered=TRUE), values=cat, opacity=1)
    
  output$mymap <- renderLeaflet({
    filtered <- feat
    if (input$filterBy=="HORA"){
      start <- input$start - years(year(Sys.Date())) - months(month(Sys.Date()) - 1) - days(day(Sys.Date()) - 1)
      end <- input$end - years(year(Sys.Date())) - months(month(Sys.Date()) - 1) - days(day(Sys.Date()) - 1)
      intv <- interval(start, end, tz='LMT')
      filtered <- filtered[which(filtered$attributes$HORA %within% intv),]
    }
    else if (input$filterBy!="Seleccionar"){
      filtered <- filtered[which(filtered$attributes[[input$filterBy]]==input[[input$filterBy]]),]
    }
    groups <- split(filtered, filtered$attributes$GRAVEDAD)
    colnames <- names(groups)
    
    sd <- if ("SOLO DAÑOS" %in% colnames) groups[["SOLO DAÑOS"]] else edf
    h <- if ("HERIDO" %in% colnames) groups[["HERIDO"]] else edf
    m <- if ("MUERTO" %in% colnames) groups[["HERIDO"]] else edf
    cm <- if ("CON MUERTO" %in% colnames) groups[["CON MUERTO"]] else edf
    
    addCircles(map, sd$geometry$x, sd$geometry$y, weight=3, radius=30,color="#0000ff", stroke=TRUE, fillOpacity=0.8, popup=sd$popup) %>%
    addCircles(h$geometry$x, h$geometry$y, weight=3, radius=30,color="#ffa500", stroke=TRUE, fillOpacity=0.8, popup=h$popup)  %>%
    addCircles(m$geometry$x, m$geometry$y, weight=3, radius=30,color="#ff0000", stroke=TRUE, fillOpacity=0.8, popup=m$popup) %>%
    addCircles(cm$geometry$x, cm$geometry$y, weight=3, radius=30,color="#000000", stroke=TRUE, fillOpacity=0.8, popup=cm$popup)
  })
}